package action;

import com.opensymphony.xwork2.ActionSupport;

public class LoginAction extends ActionSupport{
	String username;
	String password;

	public LoginAction() {
		// TODO Auto-generated constructor stub
	}

	public String execute() {
//		return "success";
		if (username.isEmpty() || password.isEmpty()) {
			return "error";
		}

		else {
			if (username.equalsIgnoreCase("admin")
					&& password.equalsIgnoreCase("admin")) {
				return "success";
			}
		}

		return "error";
	}

	/*
	 * khi dung ham validate can khai bao trong struts.xml
	 * <result name="input">url</result>
	 * tuc la neu validate loi thi se chuyen toi url
	 * */
	public void validate() {
		if (username.isEmpty()) {
			addActionError("Username can't be blanked");
		} else {
			addActionMessage("Welcome " + username
					+ ", You have been Successfully Logged in");
		}
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public boolean checkLogin() {
		if (username.isEmpty() || password.isEmpty()) {
			return false;
		} else {
			if (username.equalsIgnoreCase("admin")
					&& password.equalsIgnoreCase("admin")) {
				return true;
			}
		}

		return false;
	}

}
